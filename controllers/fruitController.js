// const {v4 : uuidv4} = require('uuid')


const {Op} = require('sequelize')
const { Fruit } = require('../models')

class FruitController {

    async list(req, res, next){
        const fruits = await Fruit.findAndCountAll({raw: true, })
        // console.log('posts',posts)0
        res.json(fruits)

    }

    async getFruitById(req, res, next){
        const {id} = req.params;
        console.log("id", id)
       
        const fruit = await Fruit.findOne({
            // raw:true,
            where:{id},
            // include: FruitType
             } )

        res.json(fruit)
        console.log(fruit)
    }

    async getFruitByCriteria(req, res, next){
        const {id, name} = req.body;

        const criteria = []
        id && criteria.push({id:id})
        name && criteria.push({name:name})

        const category = await Fruit.findOne({
            raw:true,
            where:
            {
                [Op.and]:[...criteria]
            }, 

            } )

        res.json(category)
    }

    async addFruit(req, res, next){
        const {id, name, img} = req.body
        const fruit = await Fruit.create( {id, name, img});
        // console.log("Fruit", staff)
        res.json({success: true, newFruit: fruit})
    }

    async updateFruit(req,res, next){
        const fruit = req.body
        const result = await Fruit.update(fruit, {where:{id:fruit.id}})
        res.json({result:result[0]})
    }

    async deleteFruit(req, res, next){
        const id = req.params.id        
        
        const result = await Fruit.destroy({where: {id}})
         res.json({result} )

    }
}

module.exports = new FruitController()