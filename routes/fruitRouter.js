const Router = require('express')
const { FruitController } = require('../controllers')
// const PostController =require('../controllers')
// const { authMiddleware } = require('../middlewares')

const router = new Router()

router.get('/', FruitController.list)
// router.get('/:id', PostController.getPostById)
router.get('/:id', FruitController.getFruitById)

router.post('/', FruitController.addFruit)
// // //auth routes
router.put('/', FruitController.updateFruit)
// // router.put('/', authMiddleware, PostController.updateCategory)
router.delete('/:id', FruitController.deleteFruit)

module.exports = router