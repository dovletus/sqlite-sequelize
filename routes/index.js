const Router = require('express')

const FruitRouter = require('./fruitRouter')

const router = new Router()

router.use('/fruit', FruitRouter)

module.exports = router
