// const env = require('dotenv');
// env.config();
require("dotenv").config();

const express = require("express");
const path = require('path')
const cors = require('cors')

const { sequelize } = require("./config");
const router =require('./routes')

async function startDb() {
  try {
    await sequelize.authenticate();
    console.log("Connection has been established successfully.");
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  }

  await sequelize.sync();
}

startDb();

const port = process.env.PORT || 8000;

const app = express();

app.use(cors())
app.use(express.json())


app.use('/api', router)


app.get("/a", (req, res) => res.send("OK"));

app.listen(port, () => {
  console.log("server running on", port);
});

process.on ('SIGINT', ()=>{
    sequelize.close()
}); 
